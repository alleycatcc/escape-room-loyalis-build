#!/bin/bash

set -e

cmd () {
    echo "% $@"
    "$@"
}

info () {
    echo "* $@"
}

fun () {
    echo "$@" "()"
    "$@"
}

tail-with-tag () {
    local tag=$1
    local file=$2
    local line
    cmd tail -f "$file" | while read line; do
        echo "[$tag] $line"
    done
}

forkit () {
    cmd "$@" &
}

app_env=${APP_ENV:-prd}
nginx_dir=/etc/nginx
escape_dir=/escape/escape-room-loyalis
escape_bin_dir="$escape_dir"/bin
escape_server_dir="$escape_dir"/server

clear-nginx-conf () {
    cmd rm -f "$nginx_dir"/sites-enabled/*
}

enable-nginx-conf () {
    local which=$1
    cmd cp -f "$escape_server_dir"/nginx.deploy-"$which".conf "$nginx_dir"/sites-enabled
}

# --- turn off all gzip-related directives (if any exist) in the main conf
# so we can turn them on (without errors) in the site conf.
# --- this is safer than allowing the distribution (e.g. debian) to put
# directives in the nginx conf.
# --- since we only have one site conf, there won't be any conflicts.

fix-gzip-nginx () {
    cmd sed -i 's,.\+gzip,#&,' "$nginx_dir"/nginx.conf
}

enable-gzip-nginx () {
    fun fix-gzip-nginx
    cmd cp -f "$escape_server_dir"/nginx.gzip.conf "$nginx_dir"/conf.d
}

start-nginx () {
    cmd service nginx restart
}

start-wsgi () {
    cmd "$escape_bin_dir"/start-wsgi.sh --mode=deploy
}

info "app-env: $app_env"

fun clear-nginx-conf
fun enable-nginx-conf "$app_env"
fun enable-gzip-nginx
fun start-nginx
fun forkit tail-with-tag 'nginx → access.log' /var/log/nginx/access.log
fun forkit tail-with-tag 'nginx → error.log' /var/log/nginx/error.log
fun start-wsgi
