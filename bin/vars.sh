#!/usr/bin/env bash

set -o pipefail
set -eu

rootdir="$bindir"/..

sourcesdirparts=("$rootdir" sources)
sourcesdir=$(join-out / sourcesdirparts)
defaultmodes=(main)
# --- must match name in Dockerfile
cache_break_dummy_file="$rootdir"/.dummy
# configrepo=ssh://git@gitlab.com/alleycatcc-private/loyalis-escape-room-config
# configrepostub=config

sourcesconfigdir="$sourcesdir"/loyalis-escape-room-config
sourcesconfigyamlpath="$sourcesconfigdir"/config.yml

sourcesconfigjsondir="$sourcesdir"/loyalis-escape-room-config-json
sourcesconfigjsonpath="$sourcesconfigjsondir"/config.json

declare -A imagenames
imagenames[main]=escape-loyalis-main:tst

declare -A dockerfiles
dockerfiles[main]=Dockerfile-main
