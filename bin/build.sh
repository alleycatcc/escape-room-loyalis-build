#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

. "$bindir"/vars.sh
. "$bindir"/vars-local.sh
. "$bindir"/common.sh

USAGE_STR=(
    'Usage: $0 [-C] [-i] [step ...]'
    ''
    '-i to run init step (deletes sources)'
    '-C to disable docker cache'
    '‘step ...’ defaults to ‘%s’'
)

USAGE=$(printf "$(join-out $'\n' USAGE_STR)" "${defaultmodes[*]}")

opt_C=
opt_i=
while getopts hiC-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        i) opt_i=yes ;;
        C) opt_C=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

# get-config-yaml () {
    # chd "$sourcesdir"
    # cmd git clone "$configrepo" "$configrepostub"
# }

yaml-to-json-config () {
    cmd redirect-in "$sourcesconfigyamlpath" redirect-out "$sourcesconfigjsonpath" "$bindir"/yaml-to-json.py
}

init () {
    fun safe-rm-dir-array sourcesdirparts
}

prepare-config () {
    mkd "$sourcesconfigjsondir"
    fun yaml-to-json-config
}

build () {
    local which=$1
    local imagename=$2
    local dockerfile=$3
    local nocache=${4:+--no-cache}

    info "$(printf "build-%s → %s" "$which" "$(yellow "$imagename")")"
    cwd "$rootdir" cmd docker build $nocache -t "$imagename" -f "$dockerfile" .
}

generate-cache-break-dummy () {
    cmd redirect-out "$cache_break_dummy_file" echo "$RANDOM"
}

sha1check () {
    sha1sum -c
}

go () {
    local nocache=$1; shift
    local modes
    local mode
    local imagename
    local dockerfile
    local builtnames=()
    local builtstring

    if [ $# = 0 ]; then modes=(all)
    else modes=("$@"); fi

    if [ "${modes[*]}" = all ]; then
        modes=("${defaultmodes[@]}")
    else
        fun check-modes "${modes[@]}"
    fi

    fun generate-cache-break-dummy

    for mode in "${modes[@]}"; do
        imagename=${imagenames["$mode"]}
        dockerfile=${dockerfiles["$mode"]}
        fun build "$mode" "$imagename" "$dockerfile" "$nocache"
        builtnames+=("$imagename")
    done

    builtstring=$(join-out ', ' builtnames)
    info "$(printf "Successfully built %s" "$(green "$builtstring")")"
}

if [ "$opt_i" = yes ]; then
    fun init
fi

fun prepare-config
fun go "$opt_C" "$@"
