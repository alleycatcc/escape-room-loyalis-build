# -*- coding: utf-8 -*-

import proj_paths # noqa

from acatpy.util import dirscript, pathparent, pathjoin

def get(scriptpath=dirscript()):
    rootpath = pathparent(scriptpath)
    superpath = pathparent(rootpath)
    return {}
