#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, yaml, json
json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)
