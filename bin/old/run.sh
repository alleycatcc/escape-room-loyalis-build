#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

. "$bindir"/vars.sh
. "$bindir"/vars-local.sh
. "$bindir"/common.sh

USAGE=$(printf $'Usage: $0 [-d docker-opt [-d docker-opt ...]] [unit ...]\n\nunits default to "%s"' "${defaultmodes[*]}")

opt_d=()
while getopts hd:-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        d) opt_d+=("$OPTARG") ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

run () {
    local mode=$1
    local imagename=$2
    local ports_var=$3
    local ports
    local port
    local hostport
    local portstr
    local portparam=()
    fun copy-array ports "$ports_var"
    for port in "${ports[@]}"; do
        let hostport=port+10000
        portstr="$hostport":"$port"
        portparam+=(-p "$portstr")
    done

    cmd docker run "${opt_d[@]}" "${portparam[@]}" "$imagename"
}

go () {
    local modes
    local mode
    local imagename
    local ports

    if [ $# = 0 ]; then modes=("${defaultmodes[@]}")
    else
        modes=("$@")
        fun check-modes "${modes[@]}"
    fi

    for mode in "${modes[@]}"; do
        imagename=${imagenames["$mode"]}
        info "$(printf "Starting %s" "$(yellow "$mode")")"
        fun run "$mode" "$imagename" ports_"$mode"
    done
}

fun go "$@"
