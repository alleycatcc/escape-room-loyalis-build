# Intro

This repo is likely only of interest to those involved in the
Grieks-Nederlands project.

These instructions are based on unix-like systems. Other systems should
probably work with minor modifications; suggestions welcome.

This is the build environment for GrNe. You must already have access to the various data sources (lemmata, lemmatiser) and a machine which can build/run Docker images.

The standard build gets the lemmata from an SVN repo and the lemmatiser from the alleycatcc-private GitLab repo. This behavior can be manually overriden if desired by changing `bin/vars-local.sh`.

Credentials are required to fetch the lemmata from SVN. If it is not possible to enter the credentials when prompted (e.g. for non-interactive builds), you can create a file called `bin/lemma-svn-credentials`, consisting of the username on the first line and the password on the second line. This path is already included in .gitignore. Note that this method requires SVN >= 1.10.0, because it uses the --password-from-stdin option to not expose the password on the command line.

You do not need root privileges to build the images, but depending on your
system you may need to add your user to the 'docker' group.

# Usage

## Build

### First time

    git clone https://gitlab.com/alleycatcc/grne-build.git
    cd grne-build
    git reset --hard v0.1.6
    git submodule update --init --recursive
    # --- create `bin/vars-local.sh`; modify if necessary.
    cd bin; cp vars-local-example.sh vars-local.sh; cd ..
    # --- fetch the data sources and build -- the build generates a lot of output and should take around 30-40 minutes on a not super-fast machine.
    # --- to catch the output:
    bin/build -i 2>&1 | tee /tmp/out
    # --- or just
    # bin/build -i
    
### Subsequent times, if the data hasn't changed

    # --- just rebuild.
    bin/build

## Deploy

You should now have two Docker images: `grne-main:latest` and
`grne-search:latest`.

### Search

`grne-search` is a self-contained Solr service with all the data indexed and
ready to use. It exposes a single port, 8983. Choose a local port to
forward, e.g. 9001:

    docker run -p 9001:8983 \
        --env GRNE_ENV=tst \
        -d grne-search:latest

Possible values for `GRNE_ENV` are 'tst', 'acc', or 'prod' (default if
omitted is 'tst').

Browse to http://localhost:9001/solr/#/grne/query and try the query '\*:\*'

### Main

`grne-main` is where the main web site runs (WIP), and also the 'analyse
pipeline' tool. A number of environment variables are necessary to configure
the container. To map local port 9000:

    docker run -p 9000:8080 \
        --env GRNE_ENV=tst \
        --env GRNE_SOLR_URL=http://10.12.12.32:8983/solr/grne \
        --env GRNE_SMTP_HOST=smtp.hosting.com \
        --env GRNE_SMTP_USERNAME=feedback@your-domain.nl \
        --env GRNE_SMTP_PORT=465 \
        --env GRNE_SMTP_PASSWORD=xxx \
        --env GRNE_FEEDBACK_RECIPIENT=feedback@your-domain.nl \
        -d grne-main:latest

You should be able to browse to http://localhost:9000 and http://localhost:9000/analyse

See above for possible values for `GRNE_ENV`.

`GRNE_SOLR_URL` is required -- see below.

The SMTP-related variables and `GRNE_FEEDBACK_RECIPIENT` are necessary for
the feedback form (WIP). If any of them is missing, the form will be
disabled. Secure SMTP (SSL) is required.

## Proxying server

A simple HTTP proxy will work. For Apache, the following is known to work:

    <VirtualHost *:443>
        ServerName <your-domain>
        ...
        <Location />
            ProxyPass http://localhost:9000/
            ProxyPassReverse http://localhost:9000/
        </Location>
    </VirtualHost>

## GRNE_SOLR_URL

In order for the advanced search to work, the docker containers need to be
able to communicate with each other. Specifically, your `grne-main`
container needs to know how to find `grne-search` via the `GRNE_SOLR_URL`
environment variable passed to the `grne-main` container.

The way we do this at AlleyCat is by using a docker network (see the `docker
network`) command. If you have a docker network named for example 'grne' you
can assign IP addresses to the containers on the same subnet. Example:

    docker run -p 9001:8983 \
        --network grne --ip 10.10.10.11 \
        ...
        -d grne-search:latest

    docker run -p 9000:8080 \
        --network grne --ip 10.10.10.10 \
        --env GRNE_SOLR_URL=http://10.10.10.11:8983/solr/grne \
        ...
        -d grne-main:latest
